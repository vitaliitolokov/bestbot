from pydantic_settings import BaseSettings, SettingsConfigDict
from pydantic import Field


class Settings(BaseSettings):
    TOKEN: str = None

    model_config = SettingsConfigDict(env_file='.environment', env_file_encoding='utf-8', extra='ignore')


settings = Settings()
