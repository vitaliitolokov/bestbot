from aiogram import Bot, Dispatcher
from config.settings import settings
from aiogram.enums import ParseMode
from aiogram.client.default import DefaultBotProperties

from apps.bot_v1.forms import router as form_router


async def main():
    bot = Bot(token=settings.TOKEN, default=DefaultBotProperties(parse_mode=ParseMode.HTML))
    dp = Dispatcher()
    dp.include_router(form_router)
    await dp.start_polling(bot)
