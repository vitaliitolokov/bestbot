from aiogram import Router, F
from aiogram.filters import CommandStart
from aiogram.types import Message, ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove
from aiogram.fsm.context import FSMContext

from apps.bot_v1.schemes import Form
from apps.bot_v1.messages_list import *

router = Router()


@router.message(CommandStart())
async def command_start_handler(message: Message, state: FSMContext) -> None:
    await state.set_state(Form.name)
    await message.answer(
        start,
        reply_markup=ReplyKeyboardMarkup(
            keyboard=[
                [
                    KeyboardButton(text="Да"),
                    KeyboardButton(text="Нет"),
                ]
            ],
            resize_keyboard=True,
        ),
    )


@router.message(Form.name, F.text.casefold() == "да")
async def process_dont_like_write_bots(message: Message, state: FSMContext) -> None:
    # data = await state.get_data()
    # await state.clear()
    await message.answer(
        yes,
        reply_markup=ReplyKeyboardMarkup(
            keyboard=[
                [
                    KeyboardButton(text=restart),
                ]
            ],
            resize_keyboard=True,
        ),
    )


@router.message(Form.name, F.text.casefold() == "нет")
async def process_dont_like_write_bots(message: Message, state: FSMContext) -> None:
    # data = await state.get_data()
    # await state.clear()
    await message.answer(
        no,
        reply_markup=ReplyKeyboardMarkup(
            keyboard=[
                [
                    KeyboardButton(text=restart),
                ]
            ],
            resize_keyboard=True,
        ),
    )


@router.message(Form.name, F.text.casefold() == restart.lower())
async def command_restart_bot(message: Message, state: FSMContext) -> None:
    await message.answer(end_mes, reply_markup=ReplyKeyboardRemove())
